<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

//The first few lines create an arary that stores all the environments that we’ve been talking about. For each environment we have a name (local, staging, production) and a corresponding URL
$environments = array(
    'local'       => 'ioannoushoes.test',
    'staging'     => 'etherlogic.net',
    'production'  => 'ioannoushoes.com'
  );

// Get the hostname, , figures out where we are.
$http_host = $_SERVER['HTTP_HOST'];

// Loop through $environments to see if there’s a match
foreach($environments as $environment => $hostname) {
    if (stripos($http_host, $hostname) !== FALSE) {
      define('ENVIRONMENT', $environment);
      break;
    }
  }


//If there’s not a match and you can’t find the environment, display an error message:
// Exit if ENVIRONMENT is undefined
if (!defined('ENVIRONMENT')) exit('No environment configured for this host');

//Let’s grab the file for the environment we found. Create a variable to hold the file name in it.
// Location of environment-specific configuration
$wp_db_config = 'wp-config/wp-db-' . ENVIRONMENT . '.php';


//If that file exists, include it. Otherwise, display an error message. Again, if the code hits the error, the code will quit.
// Check to see if the configuration file for the environment exists
if (file_exists(__DIR__ . '/' . $wp_db_config)) {
    require_once($wp_db_config);
  } else {
    // Exit if configuration file does not exist
    exit('No database configuration found for this host');
  }

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}


/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
