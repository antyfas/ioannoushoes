<?php 
global $smof_data;
if( !isset($data) ){
	$data = $smof_data;
}

$data = ftc_array_atts(
			array(
				/* FONTS */
				'ftc_body_font_enable_google_font'					=> 1
				,'ftc_body_font_family'								=> "BebasNeue"
				,'ftc_body_font_google'								=> "BebasNeue"
				
				,'ftc_secondary_body_font_enable_google_font'		=> 1
				,'ftc_secondary_body_font_family'					=> "Arial"
				,'ftc_secondary_body_font_google'					=> "Raleway"
				
				/* COLORS */
				,'ftc_primary_color'									=> "#f69e22"

				,'ftc_secondary_color'								=> "#444444"
				
                                ,'ftc_body_background_color'								=> "#ffffff"
				/* RESPONSIVE */
				,'ftc_responsive'									=> 1
				,'ftc_layout_fullwidth'								=> 0
				,'ftc_enable_rtl'									=> 0
				
				/* FONT SIZE */
				/* Body */
				,'ftc_font_size_body'								=> 12
				,'ftc_line_height_body'								=> 24
				
				/* Custom CSS */
				,'ftc_custom_css_code'								=> ''
		), $data);		
		
$data = apply_filters('ftc_custom_style_data', $data);

extract( $data );

/* font-body */

if( $smof_data['ftc_body_font_enable_google_font'] ){
if( isset($_GET['color']) ){
 $ftc_body_font = $data['ftc_body_font_google'];
}
else{
    $ftc_body_font  = $smof_data['ftc_body_font_google']['font-family'];
}
}

else{
    if( isset($_GET['color']) ){
 $ftc_body_font = $data['ftc_body_font_google'];
} else {
 $ftc_body_font = $data['ftc_body_font_family'];
}
}

if( $smof_data['ftc_secondary_body_font_enable_google_font'] ){
if( isset($_GET['color']) ){
    $ftc_secondary_body_font    = $data['ftc_secondary_body_font_google'];
}
else{
 $ftc_secondary_body_font = $smof_data['ftc_secondary_body_font_google']['font-family'];
}
}
else{
    if( isset($_GET['color']) ){
    $ftc_secondary_body_font    = $data['ftc_secondary_body_font_google'];
}  else {
    $ftc_secondary_body_font    = $data['ftc_secondary_body_font_family'];
}
}

?>	
	
	/*
	1. FONT FAMILY
	2. GENERAL COLORS
	*/
	
	
	/* ============= 1. FONT FAMILY ============== */

    body{
        line-height: <?php echo esc_html($ftc_line_height_body)."px"?>;
    }
	
        html, 
	body,.widget-title.heading-title,
        .widget-title.product_title,.newletter_sub_input .button.button-secondary,
        #mega_main_menu.primary ul li .mega_dropdown > li.sub-style > .item_link .link_text
	, .item-description .product_title,
	 .woocommerce div.product .product_title
     , .container #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .item_link .link_text 
     , .categories-tab-h6 .ftc-products-category ul.tabs li span.title
     , .hotdeal6 .woocommerce .product .item-description .meta_info a .button-tooltip
     , .woocommerce.widget_shopping_cart .total strong, .blog-home .product_title.blog-title.entry-title a
     , .ftc-tini-cart .woocommerce ul.product_list_widget li a
     , .woocommerce-mini-cart__buttons.buttons > a.button.wc-forward
     , .single-portfolio .meta-content .portfolio-info p, .blog-h16 .blogs article h3.product_title
     , .feature-title, .text1, .text-rote, .title-home h1, .bn-countdown h1,.testimonial-content h4.name
     , .blog-h16 a.button-readmore, .text-cate-h1 h2, h4.text-180, span.product-in-category, .ft-top h3, h4.titl-ft-mid
     , .off-canvas-cart-title .title, a.close-cart, .cart-canvas .ftc-off-canvas-cart .woocommerce ul.cart_list li a
     , .cart-canvas .ftc-off-canvas-cart span.quantity, .text1-h2 h2, .cate-h16 .item-description h3.product_title
     , .cart-canvas .ftc-off-canvas-cart .woocommerce.widget_shopping_cart .total .amount
     , .testi-h18 .wpb_wrapper h3,.ftc-cookies-popup .cookies-info-text a.cookies-title, .blogs article h3.product_title
     , .newsletterpopup .wp-newletter h2,.newletter button, .footer-mobile > div > a, .ftc-mobile-wrapper .menu-text
     , .ftc-mobile-wrapper #mega_main_menu.primary.mobile_minimized-enable > .menu_holder > .menu_inner > ul > li > .item_link > .link_content > .link_text, .footer-mobile .mobile-wishlist .ftc-my-wishlist a, .footer-mobile a span.count-wish
     , .text-bn3 h3.bold, .my-cart-lb .cart-title, .ftc-mobile-wrapper .mobile-account a
     , body .widget-container h3.widget-title.berocket_aapf_widget-title span
     {
		font-family: <?php echo esc_html($ftc_body_font) ?>;
	}
	
	#mega_main_menu.primary ul li .mega_dropdown > li.sub-style > ul.mega_dropdown,
        #mega_main_menu li.multicolumn_dropdown > .mega_dropdown > li .mega_dropdown > li,
        #mega_main_menu.primary ul li .mega_dropdown > li > .item_link .link_text,
        .info-open,
        .info-phone,
        .ftc-sb-account .ftc_login > a,
        .ftc-sb-account, a.ftc_lang.icl-en,
        .ftc-my-wishlist *,
        .dropdown-button span > span,
        body p,
        .wishlist-empty,
        div.product .social-sharing li a,
        .ftc-search form,
        .ftc-shop-cart,
        .conditions-box,
        .testimonial-content .info,
        .testimonial-content .byline,
        .widget-container ul.product-categories ul.children li a,
        .ftc-products-category ul.tabs li span.title,
        .woocommerce-pagination,
        .woocommerce-result-count,
        .woocommerce .products.list .product h3.product-name > a,
        .woocommerce-page .products.list .product h3.product-name > a,
        .woocommerce .products.list .product .price .amount,
        .woocommerce-page .products.list .product .price .amount,
        .products.list .short-description.list,
        div.product .single_variation_wrap .amount,
        div.product div[itemprop="offers"] .price .amount,
        .orderby-title,
        .blog .entry-info .entry-summary .short-content,
        .single-post .entry-info .entry-summary .short-content,
        .single-post article .post-info .info-category,
        .single-post article .post-info .info-category,
        #comments .comments-title,
        #comments .comment-metadata a,
        .post-navigation .nav-previous,
        .post-navigation .nav-next,
        .woocommerce-review-link,
        .ftc_feature_info,
        .woocommerce div.product p.stock,
        .woocommerce div.product .summary div[itemprop="description"],
        .woocommerce div.product p.price,
        .woocommerce div.product .woocommerce-tabs .panel,
        .woocommerce div.product form.cart .group_table td.label,
        .woocommerce div.product form.cart .group_table td.price,
        footer,
        footer a,
        .blogs article .image-eff:before,
        .blogs article a.gallery .owl-item:after,
        .coming_soon .sub-mailchimp .input-mail input,
        .spns-home-1 div.product_title,
        .home1.lopc2 div.product_title,
        .deal-products div.product .countdown-meta,
        .st-list-cat .category-slider.product-category .item-description .count-product-category
        , .tab-trending .vc_tta.vc_general .vc_tta-tabs-list li.vc_tta-tab a
        , .blog-home5 .blogs .entry-body .entry-content p
        , .ftc-shop-cart .dropdown-container ul.ftc_cart_list li .product-name a
        , .pv_shop_description.col-md-9, .pv_shop_description.col-md-12
        , .dokan-widget-area.widget-collapse #cat-drop-stack > ul li ul.children.level-0
        , .vendor_description a.wcmp_vendor_detail, .prod-cat-show-top-content-button
        , body.wpb-js-composer .vc_tta.vc_general .vc_tta-panel-body ul.list-unstyled
        , .deal-products .ftc-product.product .item-description h3.product_title:before
        , .header-layout3 .nav-right a.ftc-checkout-menu
        , .deal-products .ftc-product.product .item-description .short-description
        , .date-time.date-time-meta.home7, .cookies-info-text, .cookies-buttons a.cookies-more-btn
        , .dont_show_popup label
        , .test-home1 .ftc-product-time-deal .short-description
        , .header-layout11 a.ftc_lang.icl-en
        , .header-layout12 a.ftc_lang.icl-en
        , .header-layout13 a.ftc_lang.icl-en
        , .img-text1 p.text-1
        , footer .footer-middle11 ul.bullet li a
        , .blog-home.home12 .vcard.author
        , .testimonial-home4 .ftc-sb-testimonial .testimonial-content .byline
        , body .testimonial-home4 .ftc-sb-testimonial .active .testimonial-content .info
        , .cookies-buttons a.btn.btn-size-small.btn-color-primary.cookies-accept-btn 
        , .single-portfolio .portfolio-info span, .single-portfolio .portfolio-info span a
        ,.single-portfolio .related .sub-title span, .ftc_feature_info, .feature-h16 .ftc-feature .feature-content:after, .text1 p
        ,.bn1-h1 .ftc-sb-button .ftc-button, .title-home h4,  div#dropdown-list div ul li a
        , .product-tab16 .vc_general.vc_tta.vc_tta-tabs .vc_tta-tabs-list li.vc_tta-tab a
        , .bn-bt-h1 .ftc-sb-button .ftc-button , .product-tab16 div.product span.price
        , .bn-bt-h12 .ftc-sb-button .ftc-button , .bn-countdown p,.bn-countdown .ftc-countdown .counter-wrapper > div .countdown-meta
        , .bn-countdown .ftc-sb-button .ftc-button, .notslider .load-more-wrapper .load-more.button, .testimonial-content .byline
        , .testimonial-content .info, .blog-h16 .blogs .post-info .home7, .blog-h16 .blogs .post-info .entry-info
        , .blog-h16 .blogs .post-info .entry-content p, .text-cate-h1 .ftc-sb-button .ftc-button
        , .ft-top p, .ft-top-mail form .btn-sub button, blockquote
        , .ft-mid-16 ul.no-padding.bullet li a, .ft-mid-16 a,.text1-h2 p,
        body .testi-h18 .ftc-sb-testimonial .active .testimonial-content .test-home18 .info
        , .cookies-info-text, .blogs .entry-body .entry-content p, .newletter p.text, .bn1-h21 div, .bn2-h21 div
        , article a.button-readmore, .blog-timeline .date-timeline, .date-time-line, .blog-timeline .load-more-wrapper .button
        , .comments-area .commentPaginate .page-numbers, a.button-h201 , a.button-h202, a.button-h203, .header-layout20 .header-language
        , body.wpb-js-composer .product-sl21 .vc_general.vc_tta-tabs .vc_tta-tab > a
        , .product-sl21 .woocommerce div.product h3.product_title, .widget21 .item-description .product_title.product-name > a
        , .widget21 .ftc-meta-widget .meta_info .add-to-cart a, .deal21 .item-description .product_title.product-name > a
        , .deal21 .counter-wrapper div, .deal21 .counter-wrapper > div .number-wrapper .number
        , #mega_main_menu.vertical > .menu_holder > .menu_inner > ul > li > .item_link > .link_content > .link_text
        , #mega_main_menu.vertical ul li .mega_dropdown > li > .item_link .link_text, .ftc_excerpt a
        , .widget-container .berocket_aapf_widget li.slider span input, .count-view
	{
		font-family: <?php echo esc_html($ftc_secondary_body_font) ?>;
	}
	body,
        .site-footer,
        .woocommerce div.product form.cart .group_table td.label,
        .woocommerce .product .conditions-box span,
         .item-description .meta_info .yith-wcwl-add-to-wishlist a,  .item-description .meta_info .compare,
        .info-company li i,
        .social-icons .ftc-tooltip:before,
        .tagcloud a,
        .details_thumbnails .owl-nav > div:before,
        div.product .summary .yith-wcwl-add-to-wishlist a:before,
        .pp_woocommerce div.product .summary .compare:before,
        .woocommerce div.product .summary .compare:before,
        .woocommerce-page div.product .summary .compare:before,
        .woocommerce #content div.product .summary .compare:before,
        .woocommerce-page #content div.product .summary .compare:before,
        .woocommerce div.product form.cart .variations label,
        .woocommerce-page div.product form.cart .variations label,
        .pp_woocommerce div.product form.cart .variations label,
        blockquote,
        .ftc-number h3.ftc_number_meta,
        .woocommerce .widget_price_filter .price_slider_amount,
        .wishlist-empty,
        .woocommerce div.product form.cart .button,
        .woocommerce table.wishlist_table
        {
                font-size: <?php echo esc_html($ftc_font_size_body) ?>px;
        }
	/* ========== 2. GENERAL COLORS ========== */
        /* ========== Primary color ========== */
	.header-currency:hover .ftc-currency > a,
        .ftc-sb-language:hover li .ftc_lang,
        .woocommerce a.remove:hover,
        .dropdown-container .ftc_cart_check > a.button.view-cart:hover,
        .ftc-my-wishlist a:hover span,
        .nav-left .custom_content a:hover:before,
        .ftc-sb-account .ftc_login > a:hover,
        .header-currency .ftc-currency ul li:hover,
        .dropdown-button span:hover,
        body.wpb-js-composer .vc_general.vc_tta-tabs .vc_tta-tab.vc_active > a,
        body.wpb-js-composer .vc_general.vc_tta-tabs .vc_tta-tab > a:hover,
        #mega_main_menu.primary > .menu_holder.sticky_container > .menu_inner > ul > li > .item_link:hover *,
        #mega_main_menu.primary > .menu_holder.sticky_container > .menu_inner > ul > li.current-menu-item > .item_link *,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > .item_link *,
        #mega_main_menu.primary .mega_dropdown > li > .item_link:hover *,
        #mega_main_menu.primary .mega_dropdown > li.current-menu-item > .item_link *,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link *,
        .woocommerce .products .product .price,
        .woocommerce div.product p.price,
        .woocommerce div.product span.price,
        .woocommerce .products .star-rating,
        .woocommerce-page .products .star-rating,
        .star-rating:before,
        div.product div[itemprop="offers"] .price .amount,
        div.product .single_variation_wrap .amount,
        .pp_woocommerce .star-rating:before,
        .woocommerce .star-rating:before,
        .woocommerce-page .star-rating:before,
        .woocommerce-product-rating .star-rating span,
        ins .amount,
        .ftc-meta-widget .price ins,
        .ftc-meta-widget .star-rating,
        .ul-style.circle li:before,
        .woocommerce form .form-row .required,
        .blogs .comment-count i,
        .blog .comment-count i,
        .single-post .comment-count i,
        .single-post article .post-info .info-category,
        .single-post article .post-info .info-category .cat-links a,
        .single-post article .post-info .info-category .vcard.author a,
        .ftc-breadcrumb-title .ftc-breadcrumbs-content,
        .ftc-breadcrumb-title .ftc-breadcrumbs-content span.current,
        .ftc-breadcrumb-title .ftc-breadcrumbs-content a:hover,
        .woocommerce .product   .item-description .meta_info a:hover,
        .woocommerce-page .product   .item-description .meta_info a:hover,
        .ftc-meta-widget.item-description .meta_info a:hover,
        .ftc-meta-widget.item-description .meta_info .yith-wcwl-add-to-wishlist a:hover,
        .grid_list_nav a.active,
        .ftc-quickshop-wrapper .owl-nav > div.owl-next:hover,
        .ftc-quickshop-wrapper .owl-nav > div.owl-prev:hover,
        .shortcode-icon .vc_icon_element.vc_icon_element-outer .vc_icon_element-inner.vc_icon_element-color-orange .vc_icon_element-icon,
        .comment-reply-link .icon,
        body table.compare-list tr.remove td > a .remove:hover:before,
        a:hover,
        a:focus,
        .vc_toggle_title h4:hover,
        .vc_toggle_title h4:before,
        .blogs article h3.product_title a:hover,
        article .post-info a:hover,
        article .comment-content a:hover,
        .main-navigation li li.focus > a,
	.main-navigation li li:focus > a,
	.main-navigation li li:hover > a,
	.main-navigation li li a:hover,
	.main-navigation li li a:focus,
	.main-navigation li li.current_page_item a:hover,
	.main-navigation li li.current-menu-item a:hover,
	.main-navigation li li.current_page_item a:focus,
	.main-navigation li li.current-menu-item a:focus,.woocommerce-account .woocommerce-MyAccount-navigation li.is-active a, article .post-info .cat-links a,article .post-info .tags-link a,
    .vcard.author a,article .entry-header .caftc-link .cat-links a,.woocommerce-page .products.list .product h3.product-name a:hover,
    .woocommerce .products.list .product h3.product-name a:hover,
    .ftc-shop-cart .dropdown-container ul.ftc_cart_list li .product-name a:hover,
    .wpb_wrapper .tp-leftarrow.tparrows:hover:before, .wpb_wrapper .tp-rightarrow.tparrows:hover:before,
    .owl-nav > div.owl-next:hover:before, .owl-nav > div.owl-prev:hover:before,
    body .woocommerce .products .product .images .quickview:hover i:before,
    .home1.lopc2 .ftc-sub-product-categories .sub-product-categories a:hover,
    .home1 .ftc-sub-product-categories .sub-product-categories a:hover,
    .header-currency:hover .ftc-currency > a:before,
    .header-nav .nav-left .info-desc span:hover,.header-nav .nav-left .info-desc span:hover i:before,
    .ftc-sb-account .ftc-account .ftc_login:hover:before,
    .ftc-my-wishlist .tini-wishlist:hover:before,
    .ftc-sb-language .ftc_language ul li ul li a:hover span,
    .ftc-sb-account .ftc_login a.my-account:hover:before,
    .ftc-widget-post-content .author:hover i,
    #st-icon-right:hover i,
    #st-icon:hover i,
    .woocommerce .ftc-sidebar > .widget-container.ftc-items-widget .product_title.product-name:hover a,
    .ftc-sb-testimonial .testimonial-content .byline,
    .ftc-sidebar > .widget-container.ftc-product-categories-widget .ftc-product-categories-list ul li.active > a,
    .woocommerce-page .products .ftc-product.product .item-description h3 a:hover,
    body .page-container .site-content .after-loop-wrapper > .woocommerce-pagination > .page-numbers li .page-numbers:hover,
    .woocommerce .ftc-sidebar > .widget-container.ftc-product-categories-widget ul li a:hover,
    .summary.entry-summary .price .woocommerce-Price-amount.amount,
    .summary.entry-summary .price .woocommerce-Price-amount.amount,
    div.product .summary .yith-wcwl-add-to-wishlist .yith-wcwl-add-button.show a:hover, div.product .summary .yith-wcwl-add-to-wishlist .yith-wcwl-add-button.show a:hover:after,
    .woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
    .count-view span, .ftc_search_ajax .search-button:hover, .woocommerce-message::before,
	.woocommerce div.product .woocommerce-tabs ul.tabs li a:hover,
	#mega_main_menu.primary ul .mega_dropdown > li.current-menu-item > .item_link, 
	#mega_main_menu.primary ul .mega_dropdown > li > .item_link:focus, 
	#mega_main_menu.primary ul .mega_dropdown > li > .item_link:hover,
	#mega_main_menu.primary ul li.post_type_dropdown > .mega_dropdown > li > .processed_image:hover,
	.woocommerce-info::before, .blog #right-sidebar .widget-container ul > li a:hover,
	.single-post #right-sidebar  .widget-container ul > li a:hover,
	.main-navigation li li:focus > a,
	.main-navigation li li:active > a,
	.main-navigation li li:hover > a,
	.main-navigation li li a:hover,
	.main-navigation li li a:focus,
	.main-navigation li li a:active,
	.main-navigation li li.current_page_item a:hover,
	.main-navigation li li.current-menu-item a:hover,
	.main-navigation li li.current_page_item a:focus,
	.main-navigation li li.current-menu-item a:focus,
	.main-navigation li li.current_page_item a:active,
	.main-navigation li li.current-menu-item a:active,
	#mega_main_menu.primary ul .mega_dropdown > li.current-menu-item > .item_link,
	#mega_main_menu.primary ul .mega_dropdown > li > .item_link:focus, 
	#mega_main_menu.primary ul .mega_dropdown > li > .item_link:hover,
	.main-navigation li li.focus > a, .main-navigation li li:focus > a, 
	.main-navigation li li:hover > a, .main-navigation li li a:hover,
	.main-navigation li li a:focus, .main-navigation li li.current_page_item a:hover, 
	.main-navigation li li.current-menu-item a:hover, 
	.main-navigation li li.current_page_item a:focus, 
	.main-navigation li li.current-menu-item a:focus,
	#mega_main_menu.primary .mega_dropdown > li.current-menu-item > .item_link *,
	#mega_main_menu.primary .mega_dropdown > li > .item_link:focus *,
	#mega_main_menu.primary .mega_dropdown > li > .item_link:hover *,
	body .header-layout2 .container #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link *,
    body .header-layout2 #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link *,
    .header-layout2 #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > .item_link *	,
    footer .widget.widget_archive a:hover,
    footer .widget.widget_calendar a:hover,
    footer .widget.widget_categories a:hover
    ,footer .widget.widget_pages a:hover
    ,footer .widget.widget_meta a:hover
    ,footer .widget.widget_recent_comments a:hover
    ,footer .widget.widget_recent_entries a:hover
    ,footer .widget.widget_rss a:hover
    ,footer .widget.widget_search a:hover
    ,footer .widget.widget_text a:hover
    ,footer .widget.widget_nav_menu a:hover
    , .info-company li i:hover
    , .st-list-cat .category-slider.product-category .item-description h3.product_title:hover
    , .content-banner-n1 h2 span.st-content, .footer-mobile a i, .footer-mobile a:hover, .footer-mobile a:hover span.count-wish
    ,.mobile-wishlist .fa-heart, .mobile-wishlist .tini-wishlist span
    , .product-h6 .item-description .product_title.product-name > a:hover 
    , .item-description .product_title.product-name > a:hover
    , .copy-com a:hover
    , .vc_tta-container .vc_general.vc_tta-tabs .vc_tta-tabs-container .vc_tta-tabs-list li:before
    , .vc_tta-container .vc_general.vc_tta-tabs .vc_tta-tabs-container .vc_tta-tabs-list li.vc_tta-tab.vc_active:after
    , .feature-product.home8 .product_title.product-name:after
    , .blog-home8 .ftc-shortcode .header-title:after
    , .header-layout6 .ftc-shop-cart .ftc_cart .test:hover
    , .header-ftc.header-layout6 #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .item_link > .link_content > .link_text:hover
    , .widget-home5 .item-description .product_title.product-name > a:hover
    , .widget-home5 span.woocommerce-Price-amount.amount
    , .ft-mid-h8 .no-padding.info-company li a:hover
    , table.compare-list tbody tr.price.even td.odd ins span, p.woocommerce-mini-cart__buttons.buttons > a.button.wc-forward:hover
    , .mfp-close-btn-in .mfp-close:hover,
    .woocommerce ul.cart_list li a:hover,
    .woocommerce ul.product_list_widget li a:hover, .ftc_language:hover:before,
    .header-layout11 a.ftc_lang.icl-en:hover,.header-layout20 .ftc_language:hover > ul .ftc_lang,
    .ftc-off-canvas-cart .woocommerce a.remove:hover,.share-blog > i:hover,
    .woocommerce.widget_shopping_cart .cart_list li span.quantity .amount, .woocommerce.widget_shopping_cart .total .amount
    , .single-portfolio .portfolio-info span a:hover, .blog-h16 .blogs .post-info .entry-info .vcard.author:hover
    , div#dropdown-list div ul li a:hover, .bn-countdown .ftc-countdown .counter-wrapper > div .number-wrapper .number
    , .cate-home2 .item-description:hover h3.product_title, .cate-home2 .item-description:hover .product-in-category
    , .cate-h16 .item-description:hover h3.product_title, .cate-h16 .item-description:hover .product-in-category
    , .date-time-line span, a.button-h203, .wpb_text_column .wpb_wrapper .text-bn3 h3
    , .ftc-feature20 .ftc-feature:hover .feature-content > a i:before, .home20 .woocommerce .products .star-rating:before
    , .header-layout20 .ftc-cart-tini.cart-item-canvas:hover:before, .header-layout21 a.ftc-cart-tini:before
    , .header-layout21.header-ftc .header-content .ftc-shop-cart .ftc-cart-tini .cart-total
    , .header-layout21 .header-language:hover:after, .header-layout21 .header-currency:hover:after
    , .header-layout21 .header-language:hover ul li > a.ftc_lang, .header-layout21 .ftc-sb-account .ftc_login a.my-account:hover:before, .header-layout21 .ftc-my-wishlist:hover a:before, div#yith-woocompare .compare-list ins .amount
    , .ftc-enable-ajax-search .ftc-search-meta .price .amount, footer .ft-mid21 a:hover
    , #mega_main_menu.vertical > .menu_holder > .menu_inner > ul > li:hover > .item_link > .link_content > .link_text
    , #mega_main_menu.vertical > .menu_holder > .menu_inner > ul > li.menu-item-has-children:hover:after
    , #mega_main_menu.vertical ul li .mega_dropdown > li:hover > .item_link .link_text
    , #mega_main_menu.vertical > .menu_holder > .menu_inner > ul >li:hover:before
    , .woocommerce .ftc-sidebar .widget_layered_nav ul li:hover span.count
    , .woocommerce .ftc-sidebar .widget_layered_nav ul li:hover a
    , .ftc-sidebar .product-filter-by-color > ul li:hover .count, .hotspot-product .star-rating
    , .hotspot-action-hover .ftc-image-hotspot .hotspot-content .price
    , .header-ftc .header-content .ftc-shop-cart:hover .ftc-cart-tini .my-cart-lb .cart-total
    , .ftc-cart-tini.cart-lb.cart-item-canvas:hover:before, .ftc-product-video-button:hover .watch-videos
    , .ftc-product-video-button:hover:before, .dokan-category-menu #cat-drop-stack > ul li:hover a
    , #swipebox-arrows a:hover:before
    {
            color: <?php echo esc_html($ftc_primary_color) ?>;
    }
	ins .amount,  .blog-full .blogs .entry-body a.button-readmore:hover , .blog-masonry .blogs .entry-body a.button-readmore:hover
    , table.compare-list td ins .amount, .woocommerce a.remove:hover, .entry-content a, .comment-content a
    , .woocommerce a.remove:hover, body table.compare-list tr.remove td > a .remove:hover:before
	{
		color: <?php echo esc_html($ftc_primary_color) ?> !important;
	}
    .dropdown-container .ftc_cart_check > a.button.checkout:hover,
    .woocommerce .widget_price_filter .price_slider_amount .button:hover,
    .woocommerce-page .widget_price_filter .price_slider_amount .button:hover,
    body input.wpcf7-submit:hover,
    .woocommerce .products.list .product   .item-description .add-to-cart a:hover,
    .woocommerce .products.list .product   .item-description .button-in a:hover,
    .woocommerce .products.list .product   .item-description .meta_info  a:not(.quickview):hover,
    .woocommerce .products.list .product   .item-description .quickview i:hover,
    .counter-wrapper > div,
    .tp-bullets .tp-bullet:after,
    .woocommerce .product .conditions-box .onsale,
    .woocommerce #respond input#submit:hover, 
    .woocommerce a.button:hover,
    .woocommerce button.button:hover, 
    .woocommerce input.button:hover,
    .woocommerce .products .product  .images .button-in:hover a:hover,
    .woocommerce .products .product  .images a:hover,
    .vc_color-orange.vc_message_box-solid,
    .woocommerce nav.woocommerce-pagination ul li span.current,
    .woocommerce-page nav.woocommerce-pagination ul li span.current,
    .woocommerce nav.woocommerce-pagination ul li a.next:hover,
    .woocommerce-page nav.woocommerce-pagination ul li a.next:hover,
    .woocommerce nav.woocommerce-pagination ul li a.prev:hover,
    .woocommerce-page nav.woocommerce-pagination ul li a.prev:hover,
    .woocommerce nav.woocommerce-pagination ul li a:hover,
    .woocommerce-page nav.woocommerce-pagination ul li a:hover,
    .woocommerce .form-row input.button:hover,
    .load-more-wrapper .button:hover,
    body .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab:hover,
    body .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab.vc_active,
    .woocommerce div.product form.cart .button:hover,
    .woocommerce div.product div.summary p.cart a:hover,
    div.product .summary .yith-wcwl-add-to-wishlist a:hover,
    .woocommerce #content div.product .summary .compare:hover,
    div.product .social-sharing li a:hover,
    .woocommerce div.product .woocommerce-tabs ul.tabs li.active,
    .tagcloud a:hover,
    .woocommerce .wc-proceed-to-checkout a.button.alt:hover,
    .woocommerce .wc-proceed-to-checkout a.button:hover,
    .woocommerce-cart table.cart input.button:hover,
    .owl-dots > .owl-dot span:hover,
    .owl-dots > .owl-dot.active span,
    footer .style-3 .newletter_sub .button.button-secondary.transparent,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
    body .vc_tta.vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-title > a,
    body .vc_tta.vc_tta-accordion .vc_tta-panel .vc_tta-panel-title > a:hover,
    body div.pp_details a.pp_close:hover:before,
    .vc_toggle_title h4:after,
    body.error404 .page-header a,
    body .button.button-secondary,
    .pp_woocommerce div.product form.cart .button,
    .shortcode-icon .vc_icon_element.vc_icon_element-outer .vc_icon_element-inner.vc_icon_element-background-color-orange.vc_icon_element-background,
    .style1 .ftc-countdown .counter-wrapper > div,
    .style2 .ftc-countdown .counter-wrapper > div,
    .style3 .ftc-countdown .counter-wrapper > div,
    #cboxClose:hover,
    body > h1,
    table.compare-list .add-to-cart td a:hover,
    .vc_progress_bar.wpb_content_element > .vc_general.vc_single_bar > .vc_bar,
    div.product.vertical-thumbnail .details-img .owl-controls div.owl-prev:hover,
    div.product.vertical-thumbnail .details-img .owl-controls div.owl-next:hover,
    ul > .page-numbers.current,
    ul > .page-numbers:hover,
    article a.button-readmore:hover,.text_service a,.vc_toggle_title h4:before,.vc_toggle_active .vc_toggle_title h4:before,
    .post-item.sticky .post-info .entry-info .sticky-post,
    .woocommerce .products.list .product   .item-description .compare.added:hover,
    .blogs .date .date-time span,
    footer .ftc_newletter_sub .newletter_sub .button.button-secondary.transparent,
    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current_page_item, #mega_main_menu > .menu_holder > .menu_inner > ul > li:hover, #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link,
    .site-content .woocommerce .product   .item-description .meta_info a,
    .header-layout2 .navigation-primary,
    .ftc-feature .feature-content > a,
    .cd_product .home1_cd,
    .home1_produc1 .ftc-sub-product-categories span.hot, .home1_product2 .ftc-sub-product-categories span.hot,
    .ftc-sb-testimonial .owl-item:hover .testimonial-content .info,
    .ftc-sidebar > .widget-container.ftc-product-categories-widget h3.widget-title.product_title,
    .ftc-sidebar > .widget-container.widget_text,
    .after-loop-wrapper > .woocommerce-pagination > .page-numbers li .page-numbers.current,
    .related.products .ftc-product.product .item-description .meta_info > div a, .related.products .ftc-product.product .item-description .meta_info > a, .woocommerce .product .item-description .meta_info a.added_to_cart.wc-forward,
	.details_thumbnails .owl-nav .owl-prev:hover,
    .details_thumbnails .owl-nav .owl-next:hover, .woocommerce #content table.wishlist_table.cart a.remove:hover
    , .nav-links span.page-numbers.current,a.page-numbers:hover, p.form-submit input[type="submit"]:hover,
    .woocommerce div.product div.summary p.cart a,
    .woocommerce div.product form.cart .button, #today, #to-top a:hover,
    .coming_soon .sub-mailchimp .btn-sub button:hover,
    .text-footer .sub-mailchimp .btn-sub button,
    .pp_content  .product-type-external .summary.entry-summary .single_add_to_cart_button.button,
    .woocommerce table.shop_table tbody tr td.product-remove a:hover,
    #to-top a, .container #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link 
    , .ftc-mobile-wrapper .menu-text .btn-toggle-canvas.btn-danger, input[type="submit"].dokan-btn-theme
    , a.dokan-btn-theme, .dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li:hover
    , .dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.active
    , .dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.dokan-common-links a:hover
    , p.woocommerce-mini-cart__buttons.buttons > a.button.checkout.wc-forward:hover
    , .cookies-buttons a.btn.btn-size-small.btn-color-primary.cookies-accept-btn
    , .dokan-single-store .dokan-store-tabs ul li a:hover
    , form.wpcf7-form input.wpcf7-form-control.wpcf7-submit:hover
    , .woocommerce #payment #place_order:hover
    , .woocommerce table.wishlist_table tbody td.product-add-to-cart a.button:hover,
    .woocommerce-account .woocommerce-MyAccount-content .edit-account .button:hover,
    .woocommerce button.woocommerce-Button.button:hover,
    .comment-form .form-submit .submit:hover, 
     .woocommerce table.shop_table tbody tr td .button:hover
     , .ftc-portfolio-wrapper .portfolio-inner .item .thumbnail .icon-group .zoom-img
     , .ftc-portfolio-wrapper .item .icon-group div.social-portfolio
     , .single-portfolio .single-navigation a:hover:before
     ,.hotspot-btn:hover:after, .blog-h16 .owl-nav div:hover
     , .product-tab16  .product .images .group-button-product > a
     , .product-tab16  .product .images .group-button-product > div a
     , .product-tab16  .products .product .images .quickview:hover
     , .header-layout19 .header-content .header-menu-home.container
     , .bn-cd .ftc-sb-button a.ftc-button:hover, .bn1-h1 .ftc-sb-button .ftc-button:hover, .product-tab16 .ftc-sb-button a.ftc-button:hover
     , .blog-h16 a.button-readmore:hover, .notslider .load-more-wrapper .load-more.button:hover
     , .bn-countdown .ftc-sb-button .ftc-button:hover,.blog-h19 .owl-nav div:hover
     , table.compare-list .add-to-cart td a:hover, body > h1:first-child, .post-info .entry-body a.button-readmore:hover
     , article.post-wrapper:hover .date-time-line, blockquote,.blog-timeline .date-timeline:hover
     , .bn2-h20 .ftc-smooth-image, a.button-h203:hover
     , .ftc-feature20 .wpb_wrapper .ftc-feature:hover, .home20 .woocommerce .products .product .images a.compare
     ,.home8.product-slider-h9.home20 .woocommerce .product .images .group-button-product > div a
     , .header-layout20 #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor
     , .header-layout21 .nav-menu, .header-layout21 .ftc_search_ajax .search-button, .deal21 .counter-wrapper
     , .deal21 .woocommerce .product .item-description .meta_info .add-to-cart:hover a , .heading-h21 .ftc-heading h1:after
     , .widget21 .ftc-items-widget .widgettitle:after, .deal21 .header-title .product_title:after
     , .footer-middle .ft-mid21 .info-company li i, .wpb-js-composer .product-sl21 .vc_tta.vc_general .vc_tta-tab:hover:after, .wpb-js-composer .product-sl21 .vc_tta.vc_general .vc_tta-tab.vc_active:after,.home21 .ftc-sb-testimonial .owl-nav div:hover
     ,.nav-h21 .owl-nav > div:hover, .ft-top21 .owl-nav > div:hover,.deal21 .owl-nav > div:hover,.blog21 .owl-nav > div:hover,
    .widget21 .owl-nav > div:hover, .wpb-js-composer .product-sl21 .vc_tta.vc_general .vc_tta-tab:nth-child(3):hover:after
    , .wpb-js-composer .product-sl21 .vc_tta.vc_general .vc_tta-tab.vc_active:nth-child(3):after
    , table.compare-list .add-to-cart td a:hover, .header-layout20 .ftc-cart-tini:hover .cart-total
    {
        background-color: <?php echo esc_html($ftc_primary_color) ?>;
    }
	.header-layout2 #mega_main_menu.primary > .menu_holder.sticky_container > .menu_inner > ul > li.current_page_item, 
    .header-layout2 #mega_main_menu > .menu_holder.sticky_container  > .menu_inner > ul > li:hover, 
    .header-layout2 #mega_main_menu.primary > .menu_holder.sticky_container > .menu_inner > ul > li.current-menu-ancestor > .item_link
    , .blog-timeline .load-more-wrapper a.load-more.button:hover
    , .site-content .product-sl21 .woocommerce .product .item-description .meta_info a:hover
    , table.compare-list .add-to-cart td a:hover
		{
                background-color: <?php echo esc_html($ftc_primary_color) ?> !important;
        }
	.dropdown-container .ftc_cart_check > a.button.view-cart:hover,
    .dropdown-container .ftc_cart_check > a.button.checkout:hover,
    .woocommerce .widget_price_filter .price_slider_amount .button:hover,
    .woocommerce-page .widget_price_filter .price_slider_amount .button:hover,
    body input.wpcf7-submit:hover,
    .counter-wrapper > div,
    .woocommerce .products .product:hover ,
    .woocommerce-page .products .product:hover ,
    #right-sidebar .product_list_widget:hover li,
    .woocommerce .product   .item-description .meta_info a:hover,
    .woocommerce-page .product   .item-description .meta_info a:hover,
    .ftc-meta-widget.item-description .meta_info a:hover,
    .ftc-meta-widget.item-description .meta_info .yith-wcwl-add-to-wishlist a:hover,
    .woocommerce .products .product:hover ,
    .woocommerce-page .products .product:hover ,
    .ftc-products-category ul.tabs li:hover,
    .ftc-products-category ul.tabs li.current,
    body .vc_tta.vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-title > a,
    body .vc_tta.vc_tta-accordion .vc_tta-panel .vc_tta-panel-title > a:hover,
     body div.pp_details a.pp_close:hover:before,
    .wpcf7 p input:focus,
    .wpcf7 p textarea:focus,
    .woocommerce form .form-row .input-text:focus,
    body .button.button-secondary,
    .ftc-quickshop-wrapper .owl-nav > div.owl-next:hover,
    .ftc-quickshop-wrapper .owl-nav > div.owl-prev:hover,
    #cboxClose:hover, .woocommerce-account .woocommerce-MyAccount-navigation li.is-active,
    .ftc-product-items-widget .ftc-meta-widget.item-description .meta_info .compare:hover,
    .ftc-product-items-widget .ftc-meta-widget.item-description .meta_info .add_to_cart_button a:hover,
    .woocommerce .product   .item-description .meta_info .add-to-cart a:hover,
    .ftc-meta-widget.item-description .meta_info .add-to-cart a:hover,
    .woocommerce .product   .item-description .meta_info .add-to-cart a,
    .woocommerce .product .item-description .meta_info .compare,
    .woocommerce .product   .item-description .meta_info .yith-wcwl-add-to-wishlist a,
    .ftc-sb-testimonial .owl-item:hover .testimonial-content .info,
	.woocommerce .product .item-description .meta_info a.added_to_cart.wc-forward:hover,
	.details_thumbnails .owl-nav .owl-prev:hover,
    .details_thumbnails .owl-nav .owl-next:hover, .nav-links span.page-numbers.current,a.page-numbers:hover, #to-top a
    , .blog-home.home4 span.vcard.author:after, .ftc-mobile-wrapper .menu-text .btn-toggle-canvas.btn-danger
    , #mega_main_menu.primary li.default_dropdown > .mega_dropdown > .menu-item > .item_link:hover:before
    , a.dokan-btn-theme, input[type="submit"].dokan-btn-danger, p.woocommerce-mini-cart__buttons.buttons > a.button.wc-forward:hover
    , p.woocommerce-mini-cart__buttons.buttons > a.button.checkout.wc-forward:hover,
    span.page-load-status p.infinite-scroll-request:after
    , .woocommerce table.shop_table tbody tr td .button:hover, .single-portfolio .related h3.heading-title:after
    ,#mega_main_menu.primary li.default_dropdown > .mega_dropdown > .menu-item.current-menu-item > .item_link:hover:before
    , article.post-wrapper:hover .date-time-line, a.button-h203
    {
            border-color: <?php echo esc_html($ftc_primary_color) ?>;
    }
    .site-content .product-sl21 .woocommerce .product .item-description .meta_info a:hover
    {
        border-color: <?php echo esc_html($ftc_primary_color) ?> !important;
    }
    .ftc_language ul ul,
    .header-currency ul,
    .ftc-account .dropdown-container,
    .ftc-shop-cart .dropdown-container,
    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current_page_item,
    #mega_main_menu > .menu_holder > .menu_inner > ul > li:hover,
    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link,
    #mega_main_menu > .menu_holder > .menu_inner > ul > li.current_page_item > a:first-child:after,
    #mega_main_menu > .menu_holder > .menu_inner > ul > li > a:first-child:hover:before,
    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link:before,
    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current_page_item > .item_link:before,
    #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .mega_dropdown,
    .woocommerce .product .conditions-box .onsale:before,
    .woocommerce .product .conditions-box .featured:before,
    .woocommerce .product .conditions-box .out-of-stock:before,
    .ftc-shortcode .header-title .product_title:before, .site-content .related.products h2:before, .related-posts .related-post-title .product_title:before
    , .woocommerce-message, .woocommerce-info
    , .home4.heading h1:after, .header-ftc div#dropdown-list
    {
            border-top-color: <?php echo esc_html($ftc_primary_color) ?>;
    }
    .woocommerce .products.list .product:hover  .item-description:after,
    .woocommerce-page .products.list .product:hover  .item-description:after
    {
            border-left-color: <?php echo esc_html($ftc_primary_color) ?>;
    }
    footer#colophon .ftc-footer .widget-title:before,
    .woocommerce div.product .woocommerce-tabs ul.tabs,
    #customer_login h2 span:before,
    .cart_totals  h2 span:before,
    .vc_tta-container .vc_general.vc_tta-tabs .vc_tta-tabs-container .vc_tta-tabs-list li.vc_active:after,
    .product_title.product-name:after,
    .testimonial .wpb_column .ftc-heading h1:after,
    .ftc-sidebar > .widget-container.ftc-items-widget h3.widget-title.product_title:before,
    #dokan-seller-listing-wrap ul.dokan-seller-wrap li .store-content .store-info .store-data h2 a:hover,
    .related-posts.related.loading .product_title .bg-heading span:after
    , .blog-home.home4 header.entry-header:before
    {
            border-bottom-color: <?php echo esc_html($ftc_primary_color) ?>;
    }
    
    /* ========== Secondary color ========== */
    body,
    .ftc-shoppping-cart a.ftc_cart:hover,
    #mega_main_menu.primary ul li .mega_dropdown > li.sub-style > .item_link .link_text,
    .woocommerce a.remove,
    body.wpb-js-composer .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab,
    .woocommerce .products .star-rating.no-rating,
    .woocommerce-page .products .star-rating.no-rating,
    .star-rating.no-rating:before,
    .pp_woocommerce .star-rating.no-rating:before,
    .woocommerce .star-rating.no-rating:before,
    .woocommerce-page .star-rating.no-rating:before,
    .woocommerce .product .images .group-button-product > div a,
    .woocommerce .product .images .group-button-product > a, 
    .vc_progress_bar .vc_single_bar .vc_label,
    .vc_btn3.vc_btn3-size-sm.vc_btn3-style-outline,
    .vc_btn3.vc_btn3-size-sm.vc_btn3-style-outline-custom,
    .vc_btn3.vc_btn3-size-md.vc_btn3-style-outline,
    .vc_btn3.vc_btn3-size-md.vc_btn3-style-outline-custom,
    .vc_btn3.vc_btn3-size-lg.vc_btn3-style-outline,
    .vc_btn3.vc_btn3-size-lg.vc_btn3-style-outline-custom,
    .style1 .ftc-countdown .counter-wrapper > div .countdown-meta,
    .style2 .ftc-countdown .counter-wrapper > div .countdown-meta,
    .style3 .ftc-countdown .counter-wrapper > div .countdown-meta,
    .style4 .ftc-countdown .counter-wrapper > div .number-wrapper .number,
    .style4 .ftc-countdown .counter-wrapper > div .countdown-meta,
    body table.compare-list tr.remove td > a .remove:before,
    .woocommerce-page .products.list .product h3.product-name a
    {
            color: <?php echo esc_html($ftc_secondary_color) ?>;
    }
    .dropdown-container .ftc_cart_check > a.button.checkout,
    .pp_woocommerce div.product form.cart .button:hover,
    .info-company li i,
    body .button.button-secondary:hover,
    div.pp_default .pp_close, body div.pp_woocommerce.pp_pic_holder .pp_close,
    body div.ftc-product-video.pp_pic_holder .pp_close,
    body .ftc-lightbox.pp_pic_holder a.pp_close,
    #cboxClose
    {
            background-color: <?php echo esc_html($ftc_secondary_color) ?>;
    }
    .dropdown-container .ftc_cart_check > a.button.checkout,
    .pp_woocommerce div.product form.cart .button:hover,
    body .button.button-secondary:hover,
    #cboxClose
    {
            border-color: <?php echo esc_html($ftc_secondary_color) ?>;
    }
    
    /* ========== Body Background color ========== */
    body
    {
            background-color: <?php echo esc_html($ftc_body_background_color) ?>;
    }
	/* Custom CSS */
	<?php 
	if( !empty($ftc_custom_css_code) ){
		echo html_entity_decode( trim( $ftc_custom_css_code ) );
	}
	?>