<?php 
$options = array();

$options[] = array(
	'id'		=> 'role'
	,'label'	=> esc_html__('Job', 'rango')
	,'desc'		=> 'Add job for Member'
	,'type'		=> 'text'
);	
		
$options[] = array(
	'id'		=> 'profile_link'
	,'label'	=> esc_html__('Profile Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'facebook_link'
	,'label'	=> esc_html__('Facebook Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'twitter_link'
	,'label'	=> esc_html__('Twitter Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'google_plus_link'
	,'label'	=> esc_html__('Google+ Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'linkedin_link'
	,'label'	=> esc_html__('LinkedIn Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'rss_link'
	,'label'	=> esc_html__('RSS Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'dribbble_link'
	,'label'	=> esc_html__('Dribbble Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'pinterest_link'
	,'label'	=> esc_html__('Pinterest Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'instagram_link'
	,'label'	=> esc_html__('Instagram Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);			

$options[] = array(
	'id'		=> 'custom_link'
	,'label'	=> esc_html__('Custom Link', 'rango')
	,'desc'		=> ''
	,'type'		=> 'text'
);

$options[] = array(
	'id'		=> 'custom_link_icon_class'
	,'label'	=> esc_html__('Custom Link Icon Class', 'rango')
	,'desc'		=> esc_html__('Use FontAwesome Class. Ex: fa-vimeo-square', 'rango')
	,'type'		=> 'text'
);
?>